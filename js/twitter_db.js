(function ($) {
  Drupal.behaviors.twitter_db = {
    attach: function (context, settings) {
      // Check if the jQuery Plugin is installed
      if (Drupal.settings.twitter_db.jquery_plugin) {
        // Check if the Cycle plugin should be used on tweets
        if (Drupal.settings.twitter_db.cycle_enabled) {
          $('.tweets').cycle({
            fx: Drupal.settings.twitter_db.cycle_effect
          }); 
        }
      }
    }
  };
})(jQuery);

